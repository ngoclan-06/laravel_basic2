@extends('user.list')

@section('content')
    <div class="container" style = "margin-top: 15px;">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-6">
                        <h3>Thêm mới</h3>
                    </div>
                    <div class="col-md-6">
                        <a href="{{ route('users.index') }}" class = "btn btn-primary float-end">Danh sách</a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <form action="{{ route('users.store') }}" method = "POST">
                    @csrf
                    <div class="row">
                        <div class="col-md-6" style = "margin-left: 300px;">
                            <div class="form-group">
                                <strong style = " @error('mail_address') color:red; @enderror">Mail_address</strong>
                                <input type="text" name = "mail_address" style = " @error('mail_address') border:red solid; @enderror" value = "{{ old('mail_address') }}" class = "form-control" placeholder="Nhap mail">
                                @error('mail_address')
                                    <div class="white text-danger" style = "color:red;">{{ $message }}</div>
                                 @enderror
                            </div>

                            <div class="form-group">
                                <strong style = " @error('password') color:red; @enderror">Password</strong>
                                <input type="password" name = "password" style = " @error('password') border:red solid; @enderror" class = "form-control" placeholder="Nhap password">
                                @error('password')
                                    <div class="white text-danger" style = "color:red;">{{ $message }}</div>
                                 @enderror
                            </div>

                            <div class="form-group">
                                <strong style = " @error('password_confirm') color:red; @enderror">Password_confirm</strong>
                                <input type="password" name = "password_confirm" style = " @error('password_confirm') border:red solid; @enderror" class = "form-control" placeholder="Nhap password_confirm">
                                @error('password_confirm')
                                    <div class="white text-danger" style = "color:red;">{{ $message }}</div>
                                 @enderror
                            </div>

                            <div class="form-group">
                                <strong style = " @error('name') color:red; @enderror">Name</strong>
                                <input type="text" name = "name" style = " @error('name') border:red solid; @enderror" value = "{{ old('name') }}" class = "form-control" placeholder="Nhap name">
                                @error('name')
                                    <div class="white text-danger" style = "color:red;">{{ $message }}</div>
                                 @enderror
                            </div>

                            <div class="form-group">
                                <strong style = " @error('address') color:red; @enderror">Address</strong>
                                <input type="text" name = "address" style = " @error('address') border:red solid; @enderror" value = "{{ old('address') }}" class = "form-control" placeholder="Nhap address">
                                @error('address')
                                    <div class="white text-danger" style = "color:red;">{{ $message }}</div>
                                 @enderror
                            </div>

                            <div class="form-group">
                                <strong style = " @error('phone') color:red; @enderror">Phone</strong>
                                <input type="text" name = "phone" style = " @error('phone') border:red solid; @enderror" value = "{{ old('phone') }}" class = "form-control" placeholder="Nhap phone">
                                @error('phone')
                                    <div class="white text-danger" style = "color:red;">{{ $message }}</div>
                                 @enderror
                            </div>

                            <button type="submit" name="submit" class="btn btn-primary" style = "margin-top: 20px;">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    
@endsection
