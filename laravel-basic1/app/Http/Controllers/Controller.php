<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index() {
        // //collection of model instance
        // $allCategories = Category::all();

        // //array of fied data
        // $allCategoriesTransformed = $allCategories->toArray();

        // //Query builder
        // $queryBuilder = Category::query()->where('id', '>', 2)->orderByDesc('id');

        // //Execute query builder
        // $result = $queryBuilder->get();

        // dd($allCategories, $allCategoriesTransformed, $queryBuilder, $result);
        // return 'End turn';



        // $recentUpdated = Category::query()->recentUpdated()->get();

        // dd($recentUpdated);
        // return 'end turn';



        // $category = Category::first();
        // //collection of items model instance
        // $items = $category->items;
        // //Query buider of item in this category
        // $itemQueryBuilder = $category->items()->where('id', '>', 1);
        // //combined use of relation and local scope
        // $recentUpdateItems = Item::whereHas('category', function($q) {
        //     return $q->recentUpdated();
        // })
        // ->get();

        // dd($items, $itemQueryBuilder, $recentUpdateItems);
        // return 'End turn';



        // //collection of all Category
        // $categories = Category::all();

        // //create ann array with key by id and value is name
        // $plucked = $categories->pluck('name', 'id');

        // //collection filter
        // $filtered = $categories->filter(function($cat) {
        //     return $cat->id > 2;
        // });

        // //get only some field in type of Array
        // $only = $categories->only(['id', 'updated_at'])->toArray();
        // dd($plucked, $filtered, $only);
        // return 'End turn';


        //coleection of all category
        $categories = Category::all();
        //create an array with key by Id and value is name
        $plucked = $categories->pluck('name', 'id')->toArray();

        dd($plucked);
        return 'End turn';
    }
}
