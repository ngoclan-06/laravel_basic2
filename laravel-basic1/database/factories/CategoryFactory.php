<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use App\Models\Category;

class CategoryFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */

    protected $model = Category::class;
    public function definition()
    {
        return [
            'name' => $this->faker->name(),
            'description' => $this->faker->text(255),
            // 'description' => $this->faker->randomElement(),
        ];
    }
}
