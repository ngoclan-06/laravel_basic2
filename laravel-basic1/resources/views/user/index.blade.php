@extends('user.list')

@section('content')
    <div class="container" style = "margin-top: 15px;">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-6">
                        <h3>Laravel basic 2</h3>
                    </div>
                    <div class="col-md-6">
                        <a href="{{ route('users.create') }}" class = "btn btn-primary float-end" style = "margin-top: 20px;">Thêm mới</a>
                    </div>
                    <div class="col-md-6">
                        <h5 style = "color:rgb(65, 125, 236)">Danh sách</h5>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-striped">
                    <div>
                        @if (session()->has('message'))
                            <div class="alert alert-success">
                                {{ session('message') }}
                            </div>
                        @endif
                    </div>
                    <thead>
                        <tr>
                            <th style = "text-align: center;">STT</th>
                            <th style = "text-align: center;">Mail_address</th>
                            <th style = "text-align: center;">Password</th>
                            <th style = "text-align: center;">Name</th>
                            <th style = "text-align: center;">Address</th>
                            <th style = "text-align: center;">Phone</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($user as $us)
                        <tr>
                            <td>{{ ++$i }}</td>
                            <td>{{ $us->mail_address }}</td>
                            <td>{{ $us->password }}</td>
                            <td>{{ $us->name }}</td>
                            <td>{{ $us->address }}</td>
                            <td>{{ $us->phone }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            {{ $user->links() }}
        </div>
    </div>

@endsection
