<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Models categories
 * @property int id
 * @property string name4
 * @package App\Models
 */

class Category extends Model
{
    use HasFactory;

    /**
     * Initialize fillable
     */
    protected $table = 'categories';
    protected $fillable = [
        'name',
        'description',
    ];

    // public function item() {
    //     return $this->hasMany(item::class);
    // }
}
