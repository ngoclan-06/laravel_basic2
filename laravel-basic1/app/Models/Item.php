<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Models Item
 * @property int id
 * @property string name4
 * @package App\Models
 */

class Item extends Model
{
    use HasFactory;


    /**
     * Initialize fillable
     */
    protected $table = 'items';
    protected $fillable = [
        'name',
    ];

    // public function category() {
    //     return $this->belongsTo(categories::class);
    // }
}
