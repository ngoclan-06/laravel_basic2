<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Http\Requests\CreateUserRequest;
use DB;

class UserController extends Controller
{
    public function index() {

        // $user = User::orderBy('mail_address', 'asc')->paginate(20);
        $user = DB::table('users')
        ->orderBy('mail_address', 'asc')
        ->paginate(20);
        return view('user.index', compact('user'))->with('i', (request()->input('page',1) -1) *20);
    }
    

    /**
     * Show the form to create a new blog users.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('user.create');
    }
 
    /**
     * Store a new blog post.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateUserRequest $request)
    {

        $validated = $request->validated();

        if (User::handle($request)) {
            session()->flash('message', 'Thêm thành công');
            return redirect()->route('users.index');
        }
    }
}
