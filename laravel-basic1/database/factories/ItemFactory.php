<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use App\Models\Item;
use App\Models\Categoy;


class ItemFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */

    protected $model = Item::class;
    public function definition()
    {
        return [
            'name' => $this->faker->name(),
            'amount' => $this->faker->numerify('#####'),
            'category_id' => $this->faker->randomElement(['1', '2', '3', '4', '5']),
            // 'category_id' => $this->faker->randomElement(Category::select('id')->get()),
        ];
    }
}
