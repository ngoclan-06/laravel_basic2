<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Hash;
use DB;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $table = "users";
    protected $fillable = [
        'mail_address',
        'password',
        'name',
        'address',
        'phone',
    ];  

    public function handle($request) {
        return User::create([
            'mail_address' => $request ->mail_address,
            'password' => Hash::make($request ->password),
            'name' => $request ->name,
            'address' => $request ->address,
            'phone' => $request ->phone,
        ]);
    }
}
